package com.example.sselab.practice03_4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = (ListView) findViewById(R.id.listView);

        ArrayList<String> friendList = new ArrayList<String>();
        friendList.add("최우용");
        for (int i = 1; i <= 9; i++)
            friendList.add("친구" + i);

        ArrayAdapter<String> friendListAdap = new ArrayAdapter<String>(
                this,
                R.layout.item, // android.R.layout.simple_list_item_1
                R.id.name,
                friendList
        );
        listView.setAdapter(friendListAdap);
    }
}
